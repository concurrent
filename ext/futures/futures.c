/*
 * concurrent/futures - futures and lazy evaluation for Ruby
 *
 * Copyright (C) 2007  MenTaLguY <mental@rydia.net>
 *
 * This file is made available under the same terms as Ruby.
 */

#include "ruby.h"
#include "rubysig.h"
#include "intern.h"

static VALUE mConcurrent;
static VALUE mFutures;
static VALUE eAsyncError;
static VALUE cThunk;

static ID value_id;
static ID inspect_id;
static ID respond_to_p_id;

typedef struct {
  VALUE source;
  VALUE value;
} Thunk;

static VALUE thunk_value(VALUE obj, int evaluate) {
  VALUE original;

  original = obj;

  while ( CLASS_OF(obj) == cThunk ) {
    Thunk *thunk;
    Thunk copy;

    Data_Get_Struct(obj, Thunk, thunk);
    copy = *thunk;

    if (RTEST(copy.source)) {
      if (evaluate) {
        copy.value = rb_funcall(copy.source, value_id, 0);
        copy.source = Qnil;
        *thunk = copy;
      }

      if ( obj != original ) {
        Thunk *original_thunk;
        Data_Get_Struct(original, Thunk, original_thunk);
        *original_thunk = copy;
      }

      if (!evaluate) {
        break;
      }
    }

    obj = copy.value;
  }

  return obj;
}

static VALUE thunk_eval(VALUE thunk) {
  return thunk_value(thunk, 1);
}

static VALUE thunk_ground(VALUE thunk) {
  return thunk_value(thunk, 0);
}

void thunk_mark(Thunk const *thunk) {
  rb_gc_mark(thunk->source);
  rb_gc_mark(thunk->value);
}

void thunk_free(Thunk *thunk) {
  free(thunk);
}

static VALUE rb_thunk_new(VALUE klass, VALUE source) {
  Thunk *thunk;

  thunk = (Thunk *)malloc(sizeof(Thunk));

  thunk->source = source;
  thunk->value = Qnil;

  return Data_Wrap_Struct(cThunk, thunk_mark, thunk_free, thunk);
}

static VALUE wrap_exception(VALUE unused, VALUE ex) {
  rb_exc_raise(rb_funcall(eAsyncError, rb_intern("new"), 2, ex, rb_obj_as_string(ex)));
}

static VALUE rb_thunk_method_missing(int argc, VALUE *argv, VALUE self) {
  ID name;

  if ( argc < 1 ) {
    rb_raise(rb_eArgError, "Too few arguments (0 for 1)");
  }

  name = SYM2ID(argv[0]);
  self = thunk_ground(self);

  if ( CLASS_OF(self) == cThunk ) {
    if ( name == inspect_id && argc == 1 ) {
      VALUE source;
      Thunk *thunk;
      Data_Get_Struct(self, Thunk, thunk);
      rb_thread_critical = 1;
      source = thunk->source;
      rb_thread_critical = 0;
      if (RTEST(source)) {
        return rb_str_plus(rb_str_plus(rb_str_new2("#<Thunk "), rb_funcall(source, inspect_id, 0)), rb_str_new2(">"));
      }
    } else if ( name == respond_to_p_id && argc == 2 ) {
      if ( ID2SYM(inspect_id) == argv[1] ||
           ID2SYM(respond_to_p_id) == argv[1] )
      {
        return Qtrue;
      }
    }
  }

  self = rb_rescue2(thunk_eval, self, wrap_exception, Qnil, rb_cObject, 0);

  return rb_funcall3(self, name, argc-1, argv+1);
}

static VALUE rb_thunk_value(VALUE self, VALUE thunk_v) {
  return thunk_eval(thunk_v);
}

void Init_futures() {
  value_id = rb_intern("value");
  inspect_id = rb_intern("inspect");
  respond_to_p_id = rb_intern("respond_to?");

  mConcurrent = rb_define_module("Concurrent");
  mFutures = rb_define_module_under(mConcurrent, "Futures");

  eAsyncError = rb_define_class_under(mFutures, "AsyncError", rb_eRuntimeError);

  cThunk = rb_class_boot(0); /* not Qnil */
  rb_singleton_class(cThunk);
  rb_undef_alloc_func(cThunk);
  rb_const_set(mFutures, rb_intern("Thunk"), cThunk);

  rb_define_singleton_method(cThunk, "new", rb_thunk_new, 1);
  rb_define_singleton_method(cThunk, "value", rb_thunk_value, 1);
  rb_define_private_method(cThunk, "method_missing",
                           rb_thunk_method_missing, -1);
}


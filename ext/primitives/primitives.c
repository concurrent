/*
 * concurrent/primitives - assorted concurrency primitives
 *
 * Copyright (C) 2007  MenTaLguY <mental@rydia.net>
 *
 * This file is made available under the same terms as Ruby.
 */

#include <ruby.h>
#include <intern.h>
#include <rubysig.h>

static VALUE mPrimitives;
static VALUE cChannel;
static VALUE cSemaphore;
static VALUE cLatch;
static VALUE cAtomic;

typedef struct Entry_ {
  VALUE value;
  struct Entry_ *next;
} Entry;

typedef struct Queue_ {
  Entry *entries;
  Entry *last_entry;
  Entry *entry_pool;
  unsigned long size;
} Queue;

static void queue_init(Queue *queue) {
  queue->entries = NULL;
  queue->last_entry = NULL;
  queue->entry_pool = NULL;
  queue->size = 0;
}

static void queue_mark(Queue *queue) {
  Entry *entry;
  for (entry = queue->entries; entry; entry = entry->next) {
    rb_gc_mark(entry->value);
  }
}

static void free_entries(Entry *first) {
  Entry *next;
  while (first) {
    next = first->next;
    xfree(first);
    first = next;
  }
}

static void queue_finalize(Queue *queue) {
  free_entries(queue->entries);
  free_entries(queue->entry_pool);
}

static void
queue_put(Queue *queue, VALUE value)
{
  Entry *entry;

  if (queue->entry_pool) {
      entry = queue->entry_pool;
      queue->entry_pool = entry->next;
  } else {
      entry = ALLOC(Entry);
  }

  entry->value = value;
  entry->next = NULL;

  if (queue->last_entry) {
    queue->last_entry->next = entry;
  } else {
    queue->entries = entry;
  }
  queue->last_entry = entry;

  ++queue->size;
}

static void recycle_entries(Queue *queue, Entry *first_entry, Entry *last_entry)
{
#ifdef USE_MEM_POOLS
  last_entry->next = queue->entry_pool;
  queue->entry_pool = first_entry;
#else
  last_entry->next = NULL;
  free_entries(first_entry);
#endif
}

static VALUE queue_get(Queue *queue) {
  Entry *entry;
  VALUE value;

  entry = queue->entries;
  if (!entry) return Qundef;

  queue->entries = entry->next;
  if (entry == queue->last_entry) {
    queue->last_entry = NULL;
  }

  --queue->size;

  value = entry->value;
  recycle_entries(queue, entry, entry);

  return value;
}

static void queue_remove(Queue *queue, VALUE value) {
  Entry **ref;
  Entry *prev;
  Entry *entry;

  for (ref = &queue->entries, prev = NULL, entry = queue->entries;
       entry != NULL;
       ref = &entry->next, prev = entry, entry = entry->next)
  {
    if (entry->value == value) {
      *ref = entry->next;
      queue->size--;
      if (!entry->next) {
        queue->last_entry = prev;
      }
      recycle_entries(queue, entry, entry);
      break;
    }
  }
}

static int queue_is_empty(Queue *queue) {
  return !queue->entries;
}

static VALUE wake_thread(VALUE thread) {
  return rb_rescue2(rb_thread_wakeup, thread,
    NULL, Qundef, rb_eThreadError, 0);
}

static VALUE queue_wake_one(Queue *queue) {
  int saved_critical;
  VALUE waking;

  waking = Qnil;
  saved_critical = rb_thread_critical;
  rb_thread_critical = 1;
  while (queue->entries && !RTEST(waking)) {
    waking = wake_thread(queue_get(queue));
  }
  rb_thread_critical = saved_critical;

  return waking;
}

static VALUE queue_wake_all(Queue *queue) {
  int saved_critical;
  saved_critical = rb_thread_critical;
  rb_thread_critical = 1;
  while (queue->entries) {
    queue_wake_one(queue);
  }
  rb_thread_critical = saved_critical;
  return Qnil;
}

static VALUE queue_wait_inner(Queue *queue) {
  queue_put(queue, rb_thread_current());
  rb_thread_stop();
  return Qnil;
}

static VALUE queue_wait_cleanup(Queue *queue) {
  /* cleanup in case of spurious wakeups */
  queue_remove(queue, rb_thread_current());
  return Qnil;
}

typedef int (*WaitCondition)(void *data);

static void queue_wait(Queue *queue, WaitCondition condition, void *data) {
  int saved_critical;
  saved_critical = rb_thread_critical;
  rb_thread_critical = 1;
  while (condition(data)) {
    rb_ensure(queue_wait_inner, (VALUE)queue, queue_wait_cleanup, (VALUE)queue);
    rb_thread_critical = 1;
  }
  rb_thread_critical = saved_critical;
}

static void assert_no_survivors(Queue *waiting, const char *label, void *addr) {
  Entry *entry;
  for (entry = waiting->entries; entry; entry = entry->next) {
    if (RTEST(wake_thread(entry->value))) {
      rb_bug("%s %p freed with live thread(s) waiting", label, addr);
    }
  }
}

typedef struct Channel_ {
  Queue written;
  Queue readers;
} Channel;

static void channel_mark(Channel *channel) {
  queue_mark(&channel->written);
  queue_mark(&channel->readers);
}

static void channel_free(Channel *channel) {
  assert_no_survivors(&channel->readers, "channel", channel);
  queue_finalize(&channel->written);
  queue_finalize(&channel->readers);
  xfree(channel);
}

static VALUE s_channel_alloc(VALUE klass) {
  Channel *channel;
  channel = ALLOC(Channel);
  queue_init(&channel->written);
  queue_init(&channel->readers);
  return Data_Wrap_Struct(klass, channel_mark, channel_free, channel);
}

static VALUE m_channel_send(VALUE self, VALUE value) {
  Channel *channel;
  Data_Get_Struct(self, Channel, channel);
  queue_put(&channel->written, value);
  queue_wake_one(&channel->readers);
  return self;
}

static VALUE m_channel_receive(VALUE self) {
  Channel *channel;
  Data_Get_Struct(self, Channel, channel);
  queue_wait(&channel->readers,
             (WaitCondition)queue_is_empty, &channel->written);
  return queue_get(&channel->written);
}

typedef struct Semaphore_ {
  unsigned long count;
  Queue waiting;
} Semaphore;

static void semaphore_mark(Semaphore *semaphore) {
  queue_mark(&semaphore->waiting);
}

static void semaphore_free(Semaphore *semaphore) {
  assert_no_survivors(&semaphore->waiting, "semaphore", semaphore);
  queue_finalize(&semaphore->waiting);
  xfree(semaphore);
}

static VALUE s_semaphore_alloc(VALUE klass) {
  Semaphore *semaphore;
  semaphore = ALLOC(Semaphore);
  semaphore->count = 0;
  queue_init(&semaphore->waiting);
  return Data_Wrap_Struct(klass, semaphore_mark, semaphore_free, semaphore);
}

static VALUE m_semaphore_initialize(int argc, VALUE *argv, VALUE self) {
  Semaphore *semaphore;
  VALUE initial_count;
  initial_count = INT2FIX(0);
  rb_scan_args(argc, argv, "01", &initial_count);
  Data_Get_Struct(self, Semaphore, semaphore);
  semaphore->count = rb_num2ulong(initial_count);
  return Qnil;
}

static VALUE m_semaphore_up(VALUE self) {
  Semaphore *semaphore;
  Data_Get_Struct(self, Semaphore, semaphore);
  semaphore->count++;
  queue_wake_one(&semaphore->waiting);
  return self;
}

static int semaphore_count_is_zero(Semaphore *semaphore) {
  return semaphore->count == 0;
}

static VALUE m_semaphore_down(VALUE self) {
  Semaphore *semaphore;
  Data_Get_Struct(self, Semaphore, semaphore);
  queue_wait(&semaphore->waiting,
                  (WaitCondition)semaphore_count_is_zero, semaphore);
  semaphore->count--;
  return self;
}

typedef struct Latch_ {
  int is_set;
  Queue waiting;
} Latch;

static void latch_mark(Latch *latch) {
  queue_mark(&latch->waiting);
}

static void latch_free(Latch *latch) {
  assert_no_survivors(&latch->waiting, "latch", latch);
  queue_mark(&latch->waiting);
}

static VALUE s_latch_alloc(VALUE klass) {
  Latch *latch;
  latch = ALLOC(Latch);
  latch->is_set = 0;
  queue_init(&latch->waiting);
  return Data_Wrap_Struct(klass, latch_mark, latch_free, latch);
}

static VALUE m_latch_set(VALUE self) {
  Latch *latch;
  Data_Get_Struct(self, Latch, latch);
  latch->is_set = 1;
  queue_wake_all(&latch->waiting);
  return self;
}

static int latch_is_unset(Latch *latch) {
  return !latch->is_set;
}

static VALUE m_latch_wait(VALUE self) {
  Latch *latch;
  Data_Get_Struct(self, Latch, latch);
  queue_wait(&latch->waiting, (WaitCondition)latch_is_unset, latch);
  return self;
}

typedef struct Atomic_ {
  VALUE value;
} Atomic;

static void atomic_mark(Atomic *atomic) {
  rb_gc_mark(atomic->value);
}

static void atomic_free(Atomic *atomic) {
  xfree(atomic);
}

static VALUE s_atomic_alloc(VALUE klass) {
  Atomic *atomic = ALLOC(Atomic);
  atomic->value = Qnil;
  return Data_Wrap_Struct(klass, atomic_mark, atomic_free, atomic);
}

static VALUE m_atomic_initialize(int argc, VALUE *argv, VALUE self) {
  Atomic *atomic;
  Data_Get_Struct(self, Atomic, atomic);
  rb_scan_args(argc, argv, "01", &atomic->value);
  return Qnil;
}

static VALUE m_atomic_value(VALUE self) {
  Atomic *atomic;
  Data_Get_Struct(self, Atomic, atomic);
  return atomic->value;
}

static VALUE m_atomic_value_set(VALUE self, VALUE value) {
  Atomic *atomic;
  Data_Get_Struct(self, Atomic, atomic);
  atomic->value = value;
  return value;
}

static VALUE m_atomic_swap(VALUE self, VALUE value) {
  Atomic *atomic;
  VALUE old_value;
  Data_Get_Struct(self, Atomic, atomic);
  old_value = atomic->value;
  atomic->value = value;
  return old_value;
}

static VALUE m_atomic_set_if_equal(VALUE self, VALUE expected, VALUE value) {
  Atomic *atomic;
  Data_Get_Struct(self, Atomic, atomic);
  if ( atomic->value == expected ) {
    atomic->value = value;
    return Qtrue;
  } else {
    return Qfalse;
  }
}

void Init_primitives() {
  mPrimitives = rb_define_module_under(rb_define_module("Concurrent"), "Primitives");

  cChannel = rb_define_class_under(mPrimitives, "Channel", rb_cObject);
  cSemaphore = rb_define_class_under(mPrimitives, "Semaphore", rb_cObject);
  cLatch = rb_define_class_under(mPrimitives, "Latch", rb_cObject);
  cAtomic = rb_define_class_under(mPrimitives, "Atomic", rb_cObject);

  rb_define_alloc_func(cChannel, s_channel_alloc);
  rb_define_method(cChannel, "<<", m_channel_send, 1);
  rb_define_method(cChannel, "receive", m_channel_receive, 0);
  
  rb_define_alloc_func(cSemaphore, s_semaphore_alloc);
  rb_define_private_method(cSemaphore, "initialize", m_semaphore_initialize, -1);
  rb_define_method(cSemaphore, "up", m_semaphore_up, 0);
  rb_define_method(cSemaphore, "down", m_semaphore_down, 0);

  rb_define_alloc_func(cLatch, s_latch_alloc);
  rb_define_method(cLatch, "set", m_latch_set, 0);
  rb_define_method(cLatch, "wait", m_latch_wait, 0);

  rb_define_alloc_func(cAtomic, s_atomic_alloc);
  rb_define_private_method(cAtomic, "initialize", m_atomic_initialize, -1);
  rb_define_method(cAtomic, "value", m_atomic_value, 0);
  rb_define_method(cAtomic, "value=", m_atomic_value_set, 1);
  rb_define_method(cAtomic, "swap", m_atomic_swap, 1);
  rb_define_method(cAtomic, "set_if_equal", m_atomic_set_if_equal, 2);
}


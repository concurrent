module Concurrent
module Primitives

# An asynchronous pi-calculus channel.  Receivers wait for sent values to
# arrive, but senders do not wait for receivers.
#
# <b>Scheduling</b>: Unfair
#
class Channel

  # Sends a value on the channel and returns immediately.
  #
  # <b>Can Block</b>: Yes
  #
  def <<(value)
  end

  # Receives a value from the channel, waiting if a value is not already
  # available.
  #
  # <b>Can Block</b>: Yes
  #
  def receive
  end

end

# A counting semaphore.
#
# Semaphores are often used to offer multiple threads access to a limited
# number of resources, represented by an internally-maintained counter.  A
# thread calls Semaphore#down to reserve a resource, decrementing the counter,
# and returns the resource to the pool by calling Semaphore#up.
#
# A semaphore whose initial count is one, and whose count never exceeds one,
# is essentially a mutex.
#
# <b>Scheduling</b>: Unfair
#
# An alternate way of looking at a semaphore is as an asynchronous channel
# (like Channel) where the values written are ignored.  In principle, a
# semaphore could be implemented like this, where the current count is
# represented by the number of queued writes:
#
#  class Semaphore
#    def initialize(initial_count=0)
#      @channel = Channel.new
#      initial_count.times { @channel }
#    end
#
#    def down
#      @channel.receive
#      self
#    end
#
#    def up
#      @channel << nil
#      self
#    end
#  end
#
class Semaphore

def initialize(initial_count=0)
end

  # Increments the semaphore's count and returns immediately.
  #
  # <b>Can Block</b>: Yes
  # 
  def up
  end

  # Decrements the semaphore's count, or blocks and waits for it to be
  # incremented if the count is currently zero.
  #
  # <b>Can Block</b>: Yes
  #
  def down
  end
end

# A single-shot latch. 
#
# Threads calling Latch#wait will block until Latch#set is called, after
# which all calls to Latch#wait will return immediately.  Provides a
# race-free way to wait for simple conditions.
#
# <b>Scheduling</b>: Unfair
#
class Latch

  # Sets the latch, unblocking any currently waiting threads.
  #
  # <b>Can Block</b>: Yes
  #
  def set
  end

  # Waits for the latch to be set, or returns immediately if it is already
  # set.
  #
  # <b>Can Block</b>: Yes
  #
  def wait
  end

end

# Volatile provides a single cell which may be updated without blocking; 
# new values are immediately visible to all threads.
#
# <b>Scheduling</b>: N/A
#
class Volatile

  # Creates a new cell, initialized to +initial_value+
  def initialize(initial_value=nil)
  end

  # Returns the current value of the cell.
  #
  # <b>Can Block</b>: No
  #
  def value
  end

  # Sets the cell to +value+, returning +value+.
  #
  # <b>Can Block</b>: No
  #
  def value=(value)
  end

end

# Atomic is similar to Volatile, except that it also offers atomic swap
# and compare-and-set operations.
#
# <b>Scheduling</b>: N/A
#
class Atomic

  # Creates a new cell, initialized to +initial_value+.
  #
  def initialize(initial_value=nil)
  end

  # Returns the current value of the cell.
  #
  # <b>Can Block</b>: No
  #
  def value
  end

  # Sets the cell to +value+, returning +value+.
  #
  # <b>Can Block</b>: No
  #
  def value=(value)
  end

  # Similar to Atomic#value=, but exchanges the old and new values
  # atomically and returns the old value.
  #
  # <b>Can Block</b>: No
  #
  def swap(value)
  end

  # Atomically replaces the stored value with the given +value+, provided
  # that the existing value is equal to +expected+.  Returns true on success,
  # false otherwise.
  #
  # <b>Can Block</b>: No
  #
  def set_if_equal(expected, value)
  end

end

end
end

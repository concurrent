# concurrent/actors/actor - class representing actors
#
# Copyright 2007  MenTaLguY <mental@rydia.net>
#
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice,
#   thi slist of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice
#   this list of conditions and the following disclaimer in the documentatio
#   and/or other materials provided with the distribution.
# * Neither the name of the Evan Phoenix nor the names of its contributors 
#   may be used to endorse or promote products derived from this software 
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

require 'set'
require 'concurrent/primitives'
require 'concurrent/actors/case'
require 'concurrent/actors/mailbox'

module Concurrent
module Actors

ActorExit = Case.new :actor, :reason

class DeadActorError < RuntimeError
  attr_reader :actor
  def initialize(actor)
    super "Actor exited"
    @actor = actor
  end
end

class ActorExitError < DeadActorError
  attr_reader :reason
  def initialize(actor, reason)
    super actor
    @reason = reason
  end
end

class Actor
  class << self
    alias :new_from_impl :new

    def spawn(should_link=false)
      raise ArgumentError, "No block given" unless block_given?

      channel = Primitives::Channel.new

      parent_impl = current_impl
      Thread.new do
        Impl.new do |impl|
          Thread.current[:__current_actor_impl__] = impl
          if should_link
            parent_impl.send_link impl
            impl.send_link parent_impl
          end
          channel << impl.actor
          yield
        end
      end

      channel.receive
    end
    alias :new :spawn

    def spawn_linked
      raise ArgumentError, "No block given" unless block_given?
      spawn(true) { yield }
    end

    def current_impl
      Thread.current[:__current_actor_impl__] ||= Impl.new
    end
    private :current_impl

    def current
      current_impl.actor
    end

    def receive(timeout=nil)
      raise ArgumentError, "No block given" unless block_given?
      current_impl.mailbox.receive(timeout) { |filter| yield filter }
    end

    def check_exceptions
      current_impl.mailbox.check_exceptions
    end

    def link_impl(other_impl)
      impl = current_impl
      other_impl.send_link impl
      impl.send_link other_impl
      self
    end

    def unlink_impl(other_impl)
      impl = current_impl
      impl.send_unlink other_impl
      other_impl.send_unlink impl
      self
    end

    def link(other)
      raise ArgumentError, "Not an Actor" unless Actor === other
      other.link
    end

    def unlink(other)
      raise ArgumentError, "Not an Actor" unless Actor === other
      other.unlink
    end
  end

  class Impl
    attr_reader :actor
    attr_reader :mailbox

    def initialize
      @actor = Actor.new_from_impl self
      @mailbox = Mailbox.new

      @lock = Primitives::Semaphore.new 1
      @alive = true
      @trap_exit = false
      @linked = Set.new

      if block_given?
        watchdog { yield self }
      else
        thread = Thread.current
        Thread.new { watchdog { thread.join } }
      end
    end

    def watchdog
      begin
        reason = nil
        begin
          yield
        rescue ActorExitError => error
          reason = error.reason
        rescue Exception => reason
        end
      ensure
        linked = nil
        @lock.synchronize do
          @alive = false
          linked = @linked.to_a
          @linked = nil
        end
        linked.each do |impl|
          begin
            impl.send_exit(@actor, reason)
          rescue Exception
          end
        end
      end
    end
    private :watchdog

    # the following methods constitute the expected
    # interface for Actor implementations

    def trap_exit=(value)
      @lock.synchronize do
        @trap_exit = value
      end
    end

    def send_message(message)
      @mailbox << message
      self
    end

    def send_link(impl)
      @lock.synchronize do
        raise DeadActorError, self unless @alive
        @linked.add impl
      end
      self
    end

    def send_unlink(impl)
      @lock.synchronize do
        raise DeadActorError, self unless @alive
        @linked.delete actor
      end
      self
    end

    def send_exit(actor, reason)
      @lock.synchronize do
        @linked.delete actor
        if @trap_exit
          @mailbox << ActorExit[actor, reason]
        else 
          @mailbox.raise ActorExitError, actor, reason if reason
        end
      end
      self
    end
  end

  def initialize(impl)
    @impl = impl
  end

  def <<(message)
    @impl.send_message message
    self
  end

  def trap_exit=(value)
    @impl.trap_exit = !!value
    value
  end

  def link
    Actor.link_impl @impl
    self
  end

  def unlink
    Actor.unlink_impl @impl
    self
  end
end

end
end

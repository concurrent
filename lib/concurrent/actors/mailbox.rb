#
# concurrent/actors/mailbox - mailbox class supporting actor implementation
#
# Copyright 2007  MenTaLguY <mental@rydia.net>
#
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice,
#   thi slist of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice
#   this list of conditions and the following disclaimer in the documentatio
#   and/or other materials provided with the distribution.
# * Neither the name of the Evan Phoenix nor the names of its contributors 
#   may be used to endorse or promote products derived from this software 
#   without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

require 'concurrent/primitives'
require 'scheduler'

module Concurrent
module Actors

class TimeoutError < RuntimeError
end

class Actor
class Mailbox
  class Raise #:nodoc:
    def initialize(*args)
      @args = args
    end
    def raise
      Kernel.raise *@args
    end
  end

  class Timeout #:nodoc:
  end

  def initialize
    @lock = Primitives::Semaphore.new 1
    @pending_exceptions = []
    @channel = Primitives::Channel.new
    @skipped = []
  end

  # safe for multiple writers
  def <<(value)
    @channel << value
    self
  end

  def raise(*args)
    message = Raise.new *args
    @lock.synchronize do
      @pending_exceptions << message
      @channel << message
    end
  end

  def check_exceptions
    @lock.synchronize do
      @pending_exceptions.shift.raise unless @pending_exceptions.empty?
    end
  end

  # safe only for a single reader
  def receive(timeout=nil)
    filter = Filter.new
    if block_given?
      yield filter
      raise ArgumentError, "Empty filter" if filter.empty?
    else
      filter.when(Object) { |m| m }
    end

    value = nil
    action = nil

    check_exceptions

    found_at = nil
    @skipped.each_with_index do |obj, index|
      action = filter.action_for obj
      if action
        value = obj
        found_at = index
        break
      end
    end
    @skipped.delete_at found_at if found_at

    unless action
      timeout_message = nil
      timeout_event = nil

      begin
        if timeout
          timeout_message = Timeout.new
          if timeout > 0
            timeout_event = Scheduler.after_delay!(timeout) do
              @channel << timeout_message
            end
          else
            @channel << timeout_message
          end
        end

        until action
          value = @channel.receive

          case value
          when Timeout
            if value == timeout_message
              raise TimeoutError, "Receive timed out"
            else
              check_exceptions
            end
          when Raise
            check_exceptions
          else
            action = filter.action_for value
            unless action
              @skipped.push value
              check_exceptions
            end
          end
        end
      ensure
        timeout_event.cancel if timeout_event
      end
    end

    action.call value
  end

  class Filter
    def initialize
      @pairs = []
    end

    def when(pattern, &action)
      raise ArgumentError, "no block given" unless action
      @pairs.push [pattern, action]
    end

    def action_for(value)
      pair = @pairs.find { |pattern, action| pattern === value }
      pair ? pair[1] : nil
    end

    def empty?
      @pairs.empty?
    end
  end
end
end

end
end

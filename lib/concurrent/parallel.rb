#
# concurrent/parallel - data-parallel programming for Ruby
#
# Copyright (C) 2007  MenTaLguY <mental@rydia.net>
#
# This file is made available under the same terms as Ruby.
#

require 'singleton'
require 'concurrent/futures'

module Concurrent
module Parallel

# Returns the scheduler in effect for new jobs spawned in the current thread
#
def self.current_scheduler
  Thread.current[:__concurrent_parallel_scheduler__] ||=
    ThreadScheduler.instance
end

# Sets a different default scheduler for the duration of the block
#
def self.with_scheduler( scheduler ) #:yields:
  old_scheduler = Thread.current[:__concurrent_parallel_scheduler__]
  begin
    Thread.current[:__concurrent_parallel_scheduler__] = scheduler
    yield
  ensure
    Thread.current[:__concurrent_parallel_scheduler__] = old_scheduler
  end
end

# Spawns a job using the current scheduler; returns a future for the job's
# result.
#
def self.spawn( *args, &block ) #:yields: *args
  current_scheduler.spawn( *args, &block )
end

# A scheduler which spawns each job in a separate Ruby thread.
#
class ThreadScheduler
  include Singleton

  def spawn( *args, &block ) #:nodoc:
    Futures::Future.async { block.call *args }
  end
end

end
end

module Enumerable
  # Iterates over the collection in parallel, dividing it into pieces up to
  # +grain_size+ in size.  See Enumerable#each.
  #
  def parallel_each( grain_size, &block ) #:yields: obj
    parallel_spawn( grain_size ) do |slice|
      slice.each &block
    end.each do |task|
      Futures::Future.await task
    end
    self
  end

  # A functional map over the collection, performed in parallel in pieces
  # up to +grain_size+ in size.  See Enumerable#map.
  #
  def parallel_map( grain_size, &block ) #:yields: obj
    parallel_spawn( grain_size ) do |slice|
      slice.map &block
    end.inject( [] ) do |a, results|
      a.push *results
    end
  end

  # Selects objects from the collection, applying the given predicate in
  # parallel in pieces up to +grain_size+ in size.  See Enumerable#select.
  #
  def parallel_select( grain_size, &block ) #:yields: obj
    parallel_spawn( grain_size ) do |slice|
      slice.select &block
    end.inject( [] ) do |a, results|
      a.push *results
    end
  end

  # Rejects objects from the collection, applying the given predicate in
  # parallel in pieces up to +grain_size+ in size.  See Enumerable#reject.
  #
  def parallel_reject( grain_size, &block ) #:yields: obj
    parallel_spawn( grain_size ) do |slice|
      slice.reject &block
    end.inject( [] ) do |a, results|
      a.push *results
    end
  end

  # Finds the maximum element in the collection, in parallel chunks up to
  # +grain_size+ in size.  See Enumerable#max.
  #
  def parallel_max( grain_size )
    parallel_spawn( grain_size ) { |slice| slice.max }.max
  end

  # Finds the minimum element in the collection, in parallel chunks up to
  # +grain_size+ in size.  See Enumerable#min.
  #
  def parallel_min( grain_size )
    parallel_spawn( grain_size ) { |slice| slice.min }.min
  end

  # Divides the collection into objects which pass the predicate and those
  # which do not; works in parallel in chunks up to +grain_size+ objects
  # in size.  See Enumerable#partition.
  #
  def parallel_partition( grain_size, &block )
    parallel_spawn( grain_size ) do |slice|
      slice.partition &block
    end.inject( [ [], [] ] ) do |as, results|
      as[0].push *results[0]
      as[1].push *results[1]
      as
    end
  end

  # Searches the collection for objects matching the given regular expression
  # in parallel chunks up to +grain_size+ in size.  See Enumerable#grep.
  #
  def parallel_grep( re, grain_size, &block )
    parallel_spawn( grain_size ) do |slice|
      slice.grep( re, &block )
    end.inject( [] ) do |a, results|
      a.push *results
    end
  end

  # Returns true if all of the objects in the collection satisfy the given
  # predicate (or are true, if no predicate is supplied).  Searches the
  # collection in parallel in chunks up to +grain_size+ objects in size.
  # See Enumerable#all?
  #
  def parallel_all?( grain_size, &block ) #:yields: obj
    parallel_spawn( grain_size ) do |slice|
      slice.all? &block
    end.inject( true ) do |a, result|
      result & a
    end
  end

  # Returns true if any of the objects in the collection satisfy the given
  # predicate (or any are true, if no predicate is supplied).  Searches the
  # collection in parallel in chunks up to +grain_size+ objects in size.
  # See Enumerable#any?
  #
  def parallel_any?( grain_size, &block )
    parallel_spawn( grain_size ) do |slice|
      slice.any? &block
    end.inject( false ) do |a, result|
      result | a
    end
  end

  # Returns true if the collection includes obj.  Searches the collection
  # in chunks of up to +grain_size+ objects.  See Enumerable#include?
  #
  def parallel_include?( grain_size, obj )
    parallel_spawn( grain_size ) do |slice|
      slice.include? obj
    end.inject( false ) do |a, result|
      result | a
    end
  end

  # Divides the collection into subsets of up to +grain_size+ objects and
  # returns an array of them.  Delegates to Array#parallel_subsets by default.
  #
  def parallel_subsets( grain_size )
    to_a.parallel_subsets( grain_size )
  end

  # Spawns the given block as a job for each +grain_size+ sized subset of the
  # collection, returning an Array of futures for the results of each job.
  #
  def parallel_spawn( grain_size, &block ) #:yields: subset
    scheduler = Concurrent::Parallel.current_scheduler
    parallel_subsets( grain_size ).map do |subset|
      scheduler.spawn(subset, &block)
    end
  end
end

class Array
  # Divides the array into subsets of up to +grain_size+ objects and returns
  # an array of them.
  #
  def parallel_subsets( grain_size )
    if grain_size < size
      (0...(( size.to_f / grain_size ).ceil)).map do |i|
        self[i*grain_size, grain_size]
      end
    else
      [ self ]
    end
  end
end


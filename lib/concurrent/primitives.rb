#
# concurrent/primitives - assorted concurrency primitives
#
# Copyright 2007  MenTaLguY <mental@rydia.net>
#
# This file is made available under the same terms as Ruby.
#

module Concurrent
module Primitives

require 'concurrent/primitives.so'

class Semaphore

  # Decrements the semaphore's count while the block is run, incrementing
  # it again afterwards; will block if the count if the count is already zero.
  #
  # Particularly useful for using semaphores as mutexes.
  #
  # <b>Can Block</b>: Yes
  #
  def with
    down
    begin
      yield
    ensure
      up
    end
  end
  alias synchronize with
  alias exclusive with

  alias p down
  alias v up

  alias wait down
  alias signal up
end

unless defined? self::Volatile
Volatile = Atomic.dup
class Volatile
  undef swap
  undef set_if_equal
end
end

class Atomic
  alias exchange swap
end

end
end

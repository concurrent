require 'test/unit'
require 'concurrent/actors'
require 'thread'

include Concurrent::Actors
Channel = Concurrent::Primitives::Channel

class TestActors < Test::Unit::TestCase
  class A ; end
  class B ; end

  def test_current
    assert_instance_of Actor, Actor.current
  end

  def test_spawn
    c = Channel.new
    child = Actor.spawn { c << Actor.current }
    assert_equal child, c.receive
  end

  def test_receive_filter
    c = Channel.new
    child = Actor.spawn do
      Actor.receive do |f|
        f.when( B ) { |m| c << m }
      end
      Actor.receive do |f|
        f.when( A ) { |m| c << m }
      end
    end
    a = A.new
    b = B.new
    child << a
    child << b
    assert_equal b, c.receive
    assert_equal a, c.receive
  end

  def test_receive_empty_filter
    assert_raise ArgumentError do
      Actor.receive {}
    end
  end
end

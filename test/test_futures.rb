require 'test/unit'
require 'concurrent/futures'
require 'thread'

include Concurrent::Futures

class FuturesTests < Test::Unit::TestCase
  def test_promise_fulfill
    promise = Promise.new
    assert !promise.fulfilled?
    promise.fulfill 10
    assert promise.fulfilled?
    assert_equal 10, promise.future
  end

  def test_promise_fulfill_twice
    promise = Promise.new
    promise.fulfill 10
    assert_raise( AlreadyFulfilledError ) do
      promise.fulfill 20
    end
  end

  def test_promise_fail
    promise = Promise.new
    promise.fail( EOFError.new )
    value = promise.future
    assert_raise( EOFError ) do
      Future.await promise.value
    end
    assert_raise( AsyncError ) do
      value + 1
    end
  end

  def test_future
    f = Future.future { 3 }
    assert_equal 3, f
  end

  def test_future_raise
    f = Future.future { raise EOFError, "blah" }
    assert_raise( EOFError ) do
      Future.await f
    end
    assert_raise( AsyncError ) do
      f + 1
    end
  end

  def test_thunk_inspect
    t = Thunk.new Object.new
    t.inspect # should not try to call #value
  end

  def test_thunk_respond_to?
    t = Thunk.new Object.new
    assert t.respond_to?( :respond_to? )
    assert t.respond_to?( :inspect )
    assert_raise( AsyncError ) do
      t.respond_to?( :arbitrary )
    end
  end

  def test_thunk_method_missing_short_args
    t = Thunk.new Object.new
    assert_raise( ArgumentError ) do
      t.method_missing
    end
  end
end


require 'test/unit'
require 'concurrent/parallel'
require 'thread'

class TestParallel < Test::Unit::TestCase
  def check_parallel( enum, n, meth, *args, &block )
    regular = enum.send( meth, *args, &block )
    parallel = enum.send( "parallel_#{ meth }", n, *args, &block )
    assert_equal regular, parallel
  end

  def test_map
    check_parallel( 0..100, 2, "map" ) { |x| x * 2 }
  end

  def test_any?
    check_parallel( 0..100, 2, "any?" ) { |x| ( ( x + 1 ) % 48 ).zero? }
    check_parallel( 0..100, 2, "any?" )
    check_parallel( [ false ] * 100, 2, "any?" )
    check_parallel( [ true ] * 100, 2, "any?" )
  end

  def test_all?
    check_parallel( 0..100, 2, "all?" ) { |x| ( ( x + 1 ) % 48 ).zero? }
    check_parallel( 0..100, 2, "all?" )
    check_parallel( [ false ] * 100, 2, "all?" )
    check_parallel( [ true ] * 100, 2, "all?" )
  end

  def test_include?
    check_parallel( 0..100, 2, "include?", 5 )
    check_parallel( 0..100, 2, "include?", 1000 )
  end

  def test_with_too_large_grain_size
    check_parallel( 0..100, 200, "include?", 5 )
  end
end

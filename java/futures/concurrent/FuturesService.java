/***** BEGIN LICENSE BLOCK *****
 *  Version: CPL 1.0/GPL 2.0/LGPL 2.1
 * 
 *  The contents of this file are subject to the Common Public
 *  License Version 1.0 (the "License"); you may not use this file
 *  except in compliance with the License. You may obtain a copy of
 *  the License at http://www.eclipse.org/legal/cpl-v10.html
 * 
 *  Software distributed under the License is distributed on an "AS
 *  IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  rights and limitations under the License.
 * 
 *  Copyright (C) 2007 MenTaLguY <mental@rydia.net>
 *  
 *  Alternatively, the contents of this file may be used under the terms of
 *  either of the GNU General Public License Version 2 or later (the "GPL"),
 *  or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 *  in which case the provisions of the GPL or the LGPL are applicable instead
 *  of those above. If you wish to allow use of your version of this file only
 *  under the terms of either the GPL or the LGPL, and not to allow others to
 *  use your version of this file under the terms of the CPL, indicate your
 *  decision by deleting the provisions above and replace them with the notice
 *  and other provisions required by the GPL or the LGPL. If you do not delete
 *  the provisions above, a recipient may use your version of this file under
 *  the terms of any one of the CPL, the GPL or the LGPL.
 ***** END LICENSE BLOCK *****/

package concurrent;

import java.io.IOException;

import org.jruby.Ruby;
import org.jruby.RubyClass;
import org.jruby.RubyException;
import org.jruby.RubyObject;
import org.jruby.exceptions.RaiseException;
import org.jruby.runtime.Block;
import org.jruby.runtime.CallbackFactory;
import org.jruby.runtime.callback.Callback;
import org.jruby.runtime.ObjectAllocator;
import org.jruby.runtime.load.BasicLibraryService;
import org.jruby.runtime.builtin.IRubyObject;

public class FuturesService implements BasicLibraryService {
    public boolean basicLoad(final Ruby runtime) throws IOException {
        Thunk.setup(runtime);
        return true;
    }

    public static class Thunk extends RubyObject {
        private volatile IRubyObject source;
        private volatile IRubyObject value;

        Thunk(Ruby runtime, RubyClass klass, IRubyObject source) {
            super(runtime, klass, false);
            this.source = source;
            this.value = null;
        }

        public static IRubyObject newInstance(IRubyObject recv, IRubyObject source, Block block) {
            return new Thunk(recv.getRuntime(), (RubyClass)recv, source);
        }

        public static void setup(final Ruby runtime) throws IOException {
            RubyClass cThunk = RubyClass.createBootstrapMetaClass(runtime, null, null, ObjectAllocator.NOT_ALLOCATABLE_ALLOCATOR, null);
            cThunk.setMetaClass(runtime.getClass("Class"));
            runtime.getOrCreateModule("Concurrent").defineModuleUnder("Futures").setConstant("Thunk", cThunk);
            CallbackFactory cb = runtime.callbackFactory(Thunk.class);
            cThunk.getSingletonClass().defineMethod("new", cb.getSingletonMethod("newInstance", IRubyObject.class));
            cThunk.getSingletonClass().defineMethod("value", cb.getSingletonMethod("value", IRubyObject.class));
            cThunk.defineMethod("method_missing", cb.getOptMethod("methodMissing"));
        }

        public static IRubyObject thunkValue(IRubyObject obj, boolean evaluate) {
            IRubyObject original=obj;

            while (obj instanceof Thunk) {
                Thunk thunk=(Thunk)obj;

                if ( thunk.value == null ) {
                    if (evaluate) {
                        thunk.value = thunk.source.callMethod(thunk.source.getRuntime().getCurrentContext(), "value");
                        thunk.source = null;
                    }

                    if ( obj != original ) {
                        Thunk original_thunk = (Thunk)original;
                        original_thunk.value = thunk.value;
                    }

                    if (!evaluate) {
                        break;
                    }
                }

                obj = thunk.value;
            }

            return obj;
        }

        public static IRubyObject evalThunk(IRubyObject obj) {
            try {
                return thunkValue(obj, true);
            } catch (RaiseException e) {
                RubyClass cAsyncError = obj.getRuntime().getModule("Concurrent").defineModuleUnder("Futures").getClass("AsyncError");
                RubyException e2 = (RubyException)cAsyncError.callMethod(obj.getRuntime().getCurrentContext(), "new", e.getException());
                throw new RaiseException(e2);
            }
        }

        public static IRubyObject value(IRubyObject recv, IRubyObject obj, Block block) {
            return thunkValue(obj, true);
        }

        public IRubyObject methodMissing(IRubyObject args[], Block block) {
            if (args.length < 1) {
                throw getRuntime().newArgumentError("Too few arguments (0 for 1)");
            }

            String name = args[0].asSymbol();
            IRubyObject self = thunkValue(this, false);

            if ( self instanceof Thunk ) {
                if ( name == "inspect" && args.length == 1 ) {
                    Thunk thunk = (Thunk)self;
                    IRubyObject source;
                    source = thunk.source;
                    if ( source != null ) {
                        Ruby runtime = getRuntime();
                        return runtime.newString("#<Thunk " + source.callMethod(runtime.getCurrentContext(), "inspect").asString().toString() + ">");
                    }
                } else if ( name == "respond_to?" && args.length == 2 ) {
                    if ( args[1].asSymbol() == "inspect" || args[1].asSymbol() == "respond_to?" ) {
                        return getRuntime().getTrue();
                    }
                }
            }

            IRubyObject[] args2 = new IRubyObject[args.length-1];
            for ( int i = 0 ; i < args2.length ; i++ ) {
                args2[i] = args[i+1];
            }
            return evalThunk(this).callMethod(getRuntime().getCurrentContext(), name, args2, block);
        }
    }
}

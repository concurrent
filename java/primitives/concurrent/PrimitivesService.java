/***** BEGIN LICENSE BLOCK *****
 *  Version: CPL 1.0/GPL 2.0/LGPL 2.1
 * 
 *  The contents of this file are subject to the Common Public
 *  License Version 1.0 (the "License"); you may not use this file
 *  except in compliance with the License. You may obtain a copy of
 *  the License at http://www.eclipse.org/legal/cpl-v10.html
 * 
 *  Software distributed under the License is distributed on an "AS
 *  IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  rights and limitations under the License.
 * 
 *  Copyright (C) 2007 MenTaLguY <mental@rydia.net>
 *  
 *  Alternatively, the contents of this file may be used under the terms of
 *  either of the GNU General Public License Version 2 or later (the "GPL"),
 *  or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 *  in which case the provisions of the GPL or the LGPL are applicable instead
 *  of those above. If you wish to allow use of your version of this file only
 *  under the terms of either the GPL or the LGPL, and not to allow others to
 *  use your version of this file under the terms of the CPL, indicate your
 *  decision by deleting the provisions above and replace them with the notice
 *  and other provisions required by the GPL or the LGPL. If you do not delete
 *  the provisions above, a recipient may use your version of this file under
 *  the terms of any one of the CPL, the GPL or the LGPL.
 ***** END LICENSE BLOCK *****/

package concurrent;

import java.io.IOException;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;

import org.jruby.Ruby;
import org.jruby.RubyClass;
import org.jruby.RubyException;
import org.jruby.RubyObject;
import org.jruby.exceptions.RaiseException;
import org.jruby.runtime.Block;
import org.jruby.runtime.CallbackFactory;
import org.jruby.runtime.callback.Callback;
import org.jruby.runtime.ObjectAllocator;
import org.jruby.runtime.load.BasicLibraryService;
import org.jruby.runtime.builtin.IRubyObject;

public class PrimitivesService implements BasicLibraryService {
    public boolean basicLoad(final Ruby runtime) throws IOException {
        Channel.setup(runtime);
        Semaphore.setup(runtime);
        Latch.setup(runtime);
        Volatile.setup(runtime);
        Atomic.setup(runtime);
        return true;
    }

    public static class Channel extends RubyObject {
        private LinkedBlockingQueue<IRubyObject> contents;

        Channel(Ruby runtime, RubyClass klass) {
            super(runtime, klass, false);
            contents = new LinkedBlockingQueue<IRubyObject>();
        }

        private static ObjectAllocator ALLOCATOR = new ObjectAllocator() {
            public IRubyObject allocate(Ruby runtime, RubyClass klass) {
                return new Channel(runtime, klass);
            }
        };

        public static void setup(final Ruby runtime) throws IOException {
            CallbackFactory callbackFactory = runtime.callbackFactory(Channel.class);
            RubyClass klass = runtime.getModule("Concurrent").defineModuleUnder("Primitives").defineClassUnder("Channel", runtime.getClass("Object"), ALLOCATOR);
            klass.defineFastMethod("<<", callbackFactory.getFastMethod("send", IRubyObject.class));
            klass.defineFastMethod("receive", callbackFactory.getFastMethod("receive"));
        }

        public IRubyObject send(IRubyObject value) {
            try {
                contents.put(value);
            } catch (InterruptedException e) {
                // shouldn't happen, but...
                Thread.currentThread().interrupt();
            }
            return this;
        }

        public IRubyObject receive() {
            boolean interrupted = false;
            try {
                while (true) {
                    try {
                        return contents.take();
                    } catch (InterruptedException e) {
                        interrupted = true;
                    }
                }
            } finally {
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public static class Semaphore extends RubyObject {
        private java.util.concurrent.Semaphore semaphore = new java.util.concurrent.Semaphore(0);

        Semaphore(Ruby runtime, RubyClass klass) {
            super(runtime, klass, false);
        }

        private static ObjectAllocator ALLOCATOR = new ObjectAllocator() {
            public IRubyObject allocate(Ruby runtime, RubyClass klass) {
                return new Semaphore(runtime, klass);
            }
        };

        public static void setup(final Ruby runtime) throws IOException {
            CallbackFactory callbackFactory = runtime.callbackFactory(Semaphore.class);
            RubyClass klass = runtime.getModule("Concurrent").defineModuleUnder("Primitives").defineClassUnder("Semaphore", runtime.getClass("Object"), ALLOCATOR);
            klass.defineMethod("initialize", callbackFactory.getOptMethod("initialize"));
            klass.defineFastMethod("up", callbackFactory.getFastMethod("up"));
            klass.defineFastMethod("down", callbackFactory.getFastMethod("down"));
        }

        public IRubyObject initialize(IRubyObject[] args, Block block) {
            switch (args.length) {
            case 0: break; 
            case 1: {
                long count = args[0].convertToInteger().getLongValue();
                if ( count < 0 ) {
                } else if ( count > Integer.MAX_VALUE ) {
                }
                semaphore.release((int)count);
            } break;
            default:
            }
            return getRuntime().getNil();
        }

        public IRubyObject up() {
            semaphore.release();
            return this;
        }

        public IRubyObject down() {
            boolean interrupted = false;
            try {
                while (true) {
                    try {
                        semaphore.acquire();
                        return this;
                    } catch (InterruptedException e) {
                        interrupted = true;
                    }
                }
            } finally {
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public static class Latch extends RubyObject {
        private boolean isSet=false;

        Latch(Ruby runtime, RubyClass klass) {
            super(runtime, klass, false);
        }

        private static ObjectAllocator ALLOCATOR = new ObjectAllocator() {
            public IRubyObject allocate(Ruby runtime, RubyClass klass) {
                return new Latch(runtime, klass);
            }
        };

        public static void setup(final Ruby runtime) throws IOException {
            CallbackFactory callbackFactory = runtime.callbackFactory(Latch.class);
            RubyClass klass = runtime.getModule("Concurrent").defineModuleUnder("Primitives").defineClassUnder("Latch", runtime.getClass("Object"), ALLOCATOR);
            klass.defineFastMethod("set", callbackFactory.getFastMethod("set"));
            klass.defineFastMethod("wait", callbackFactory.getFastMethod("rubyWait"));
        }

        public synchronized IRubyObject set() {
            isSet = true;
            notifyAll();
            return this;
        }

        public synchronized IRubyObject rubyWait() {
            boolean interrupted = false;
            try {
                while (!isSet) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        interrupted = true;
                    }
                }
            } finally {
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
            }
            return this;
        }
    }

    public static class Volatile extends RubyObject {
        private volatile IRubyObject value;

        Volatile(Ruby runtime, RubyClass klass) {
            super(runtime, klass, false);
            this.value = runtime.getNil();
        }

        private static ObjectAllocator ALLOCATOR = new ObjectAllocator() {
            public IRubyObject allocate(Ruby runtime, RubyClass klass) {
                return new Volatile(runtime, klass);
            }
        };

        public static void setup(final Ruby runtime) throws IOException {
            CallbackFactory callbackFactory = runtime.callbackFactory(Volatile.class);
            RubyClass klass = runtime.getModule("Concurrent").defineModuleUnder("Primitives").defineClassUnder("Volatile", runtime.getClass("Object"), ALLOCATOR);
            klass.defineMethod("initialize", callbackFactory.getOptMethod("initialize"));
            klass.defineFastMethod("value", callbackFactory.getFastMethod("get"));
            klass.defineFastMethod("value=", callbackFactory.getFastMethod("set", IRubyObject.class));
        }

        public IRubyObject initialize(IRubyObject[] args, Block block) {
            switch (args.length) {
            case 0: break; 
            case 1: {
                this.value = args[0];
            } break;
            default:
            }
            return getRuntime().getNil();
        }

        public IRubyObject get() {
            return this.value;
        }

        public IRubyObject set(IRubyObject value) {
            this.value = value;
            return value;
        }
    }

    public static class Atomic extends RubyObject {
        private AtomicReference<IRubyObject> value = new AtomicReference<IRubyObject>();

        Atomic(Ruby runtime, RubyClass klass) {
            super(runtime, klass, false);
            this.value.set(runtime.getNil());
        }

        private static ObjectAllocator ALLOCATOR = new ObjectAllocator() {
            public IRubyObject allocate(Ruby runtime, RubyClass klass) {
                return new Atomic(runtime, klass);
            }
        };

        public static void setup(final Ruby runtime) throws IOException {
            CallbackFactory callbackFactory = runtime.callbackFactory(Atomic.class);
            RubyClass klass = runtime.getModule("Concurrent").defineModuleUnder("Primitives").defineClassUnder("Atomic", runtime.getClass("Object"), ALLOCATOR);
            klass.defineMethod("initialize", callbackFactory.getOptMethod("initialize"));
            klass.defineFastMethod("value", callbackFactory.getFastMethod("get"));
            klass.defineFastMethod("value=", callbackFactory.getFastMethod("set", IRubyObject.class));
            klass.defineFastMethod("swap", callbackFactory.getFastMethod("swap", IRubyObject.class));
            klass.defineFastMethod("set_if_equal", callbackFactory.getFastMethod("setIfEqual", IRubyObject.class, IRubyObject.class));
        }

        public IRubyObject initialize(IRubyObject[] args, Block block) {
            switch (args.length) {
            case 0: break; 
            case 1: {
                this.value.set(args[0]);
            } break;
            default:
            }
            return getRuntime().getNil();
        }

        public IRubyObject get() {
            return this.value.get();
        }

        public IRubyObject set(IRubyObject value) {
            this.value.set(value);
            return value;
        }

        public IRubyObject swap(IRubyObject value) {
            return this.value.getAndSet(value);
        }

        public IRubyObject setIfEqual(IRubyObject expected, IRubyObject value) {
            if (this.value.compareAndSet(expected, value)) {
                return getRuntime().getTrue();
            } else {
                return getRuntime().getFalse();
            }
        }
    }
}
